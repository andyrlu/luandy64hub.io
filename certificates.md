---
title: Certificates
subtitle: a place to store certificates I've gotten, mainly from freeCodeCamp
layout: page
icon: fa-trophy
order: 4
---

[Javascript Algorithms and Data Structures](https://www.freecodecamp.org/certification/luandy64/javascript-algorithms-and-data-structures)