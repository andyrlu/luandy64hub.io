---
title: Reading List
subtitle: Words I want to read and have read
layout: "page"
icon: fa-book
order: 3
---
## Books
### Currently Reading:

### Been there, done that:

## Articles
[Balacing Time](https://css-tricks.com/balancing-time/) by Sarah Drasner