---
layout: post
title:  "First Blog Post!"
date:   2017-12-14 14:59:39 -0600
categories: jekyll update
excerpt_separator: <!--more-->
icon: fa-arrow-circle-right
---
The title says it all!

I finally got this site up and running. I could not have done it without a lot 
of googling and stumbling upon Jekyll. 
<!--more-->
I hope to add more to this site as time
moves on. For the rest of this break, I hope to get a good amount of work in.
However, once the semester starts up again, I know things will get busy and 
work here will slow down.