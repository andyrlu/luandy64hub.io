---
title: My Studio Commandments
layout: post
---
Inspired by [this
post](https://superyesmore.com/studio-commandments-a5300cdf50b8a30ec909955b119f9174)
I read by Sarah Drasner, I thought it would be a really good idea to list my
"studio commandments". Just like Sarah, I can see how these would change over
time. So while this is not what I would've asked of myself in college, I think
these rules fit my work style these days.

I also like that this serves as a great snapshot of how my work style changes
with time. I am curious to see which rules persist or which rules get heavily
modified. 

Without further ado,

### My Rules
1. Start each day with a cup of coffee
    * Okay, I lied, this one speaks to younger me too. I'm sure this one is also
      not going anywhere any time soon

2. Keep water in arms reach, refill when empty
    * This is a dual purpose rule. One is to counter any diuretic effects of
      caffeine
    * Two is to make sure I get up every now and tehn to walk. I typically go
      through 24 oz. (~710 mL for you metric folks) per 3 hours
  
3. Race yourself: Set a crazy sounding time to finish a chunck of work/task and
   try to achieve it
    * I think this helps in trying to push me to work smarter. Best case
   scenario I do what I set out to do; worst case scenario I spend a little more
   time on the task and get it done anyway
   
4. Getting distracted is okay, just don't get too distracted
    * I know it's inevitable, but once I notice that I am distracted (obviously
      easier said than done), I can start making decisions to get back to work
      
5. Make notes of the process/steps of whatever task you do. For yourself and for
   others in the same boat as you
    * I know how it feels to reach some complicated documentation and feel
      discouraged that it is a pain to read and try an comprehend. So this one
      stems a bit from the boyscout rule: I want to leave the internet a little
      better than when I found it
      
I will take some time to read over these before I work, just to make sure it
really sticks. I am curious to see what is wishful thinking or what is a rule I
choose to live by.

This all seems like I need to follow up with a post about work life balance
means to me. Maybe tomorrow's post can be about that!


