---
title: i3 and Hugo Updates
date: 2018-09-05 18:00:00 -0500
layout: post
---

## Hugo Updates
I spent a nice chunk of today figuring out what I wanted to do as far as styling goes. The theme I picked, `Hyde`, was did a 
majority of the work already. 

I just had to hook into the snippet of HTML that defines the `<head>` and add my own stylesheet. From there, the only change I 
wanted to make was making everything a card. But then that led to some padding issues, so I patched that really quickly.

Overall, I'm really satisfied with the design. Now I just have to move over all of the pages and blog posts. I learned that I 
was pretty inconsistent with the front matter I included in the blog posts. I'll just have to go back and clean things up. I 
will probably automate the adding and removing of the keys, but will have to add the values afterwards. 

## i3 Updates
I've been gluing together bits and pieces of various configurations to get i3 to work. Some parts were quick:
* The background was straight forward
* Setting up my workspaces was easy
  - Though, I haven't bound certain applications to my named workspaces yet
* Installing Font Awesome was much easier than Source Code Pro. I might have to find an alternative here. It's mainly for   
  Spacemacs, but it seems to function fine without it.
* I've themed i3 to match the Nord Theme for Spacemacs

The only- what I would call- dire issue is I can't get the brightness control to work. So as I sit here and type this, there's this blindingly bright screen that I'm forced to look at and can do nothing about. 

## Next Steps
I put some config stuff from my Mac onto Github, but I later learned, I didn't put some of the files I needed. I'm hoping to 
finish up with all of this i3 stuff soon and I can throw all of those files up too. I think a really nice guide is due too 
because I wouldn't want someone else to try and do the same thing here and be as lost as I was.
