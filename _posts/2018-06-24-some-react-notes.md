---
title: Some React Notes
---
I just wanted to summerize and reflect on some notes from Day 5, 10, and 11 of
my `#100DaysOfCode` challenge on Twitter. 

So far, React has been fairly easy to pick up. It started with introducing this
idea of a `JSX Element`, which is a mix of HTML and JavaScript that wouldn't
work as pure HTML or pure JavaScript. There are these ideas of nesting the JSX
elements and `state`. 

I think the easiest way of wrapping your head around React is to treat it like
OOP; I mean, there's only two ways (that I know of right now) to declare JSX
elements. The analogy is only strengthened by the fact that JSX comes in the 
form of a function or a class. Add to that variables or constants that capture
the state of the data and you really do just have a class in the OOP sense of 
things.

Right now I have two questions that have yet to be answered:
  1. freeCodeCamp seems to always define an "inner" element before the "outer"
     element. I wonder if that is a JavaScript thing and just how it needs to 
     be done or if it's a React thing.
     
  2. I'm curious how React fits into a project and where it is written in terms
     of files. I'm picturing your average website with HTML, CSS, and JavaScript
     separated into different files. Then you add hooks in the HTML to call
     JavaScript functions that create the JSX elements or change based on the
     state. I just don't know where the functions calls to update the DOM go.

