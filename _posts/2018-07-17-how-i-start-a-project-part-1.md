---
title: How I start a project Part 1
layout: post
---
Scratching my head thinking about what I want to write about today, I started 
skimming other blogs to get a sense of content and blogging frequency. Daily 
blogging might be too much to be sustainable over time, but my rationale is I 
learn something almost every day, so that's what I'll write about.

Today, I'm documenting my process of planning a project.

So I've been working through freeCodeCamp's certificates. As of today, I only 
have the `JavaScript Data Structures and Algorithms` one done. I'm about half 
way through the `Responsive Web Design` certificate. 

While working on the RWD cert, I've realized that I spend too much time trying 
to make things perfect and that hinders my progress. Switching to the thought 
that things just need to be "good enough", I was able to finish the previous 
site, the Product Landing Page. It still needs some tweaks, which I got from 
feedback from the forums, but at least it's out there.

So here's my first stab at the Technical Documentation Page.

> My background noise of choice lately has been "Mellow Beats" or "Chill Lofi 
Study Beats" on Spotify. Find those in "Genres & Moods" > "Focus".


Now taking a look at the user stories, which comes from Agile if you are new to 
that term. A quick and dirty definition is something an end user can notice 
about the app/product. Here's the list for the 
[Technical Documentation Page](https://learn.freecodecamp.org/responsive-web-design/responsive-web-design-projects/build-a-technical-documentation-page).

Reading this document, I like to do a quick skim through first. In this pass 
through, I'm just getting a sense of how things should be laid out. There's no 
need to get bogged down by what `id`'s are required, or what the styling needs 
to look like.

Here's what caught my eye reading through this:
* There's a `main` element
* There are several `section` elements
* Something something `header`
* I was told how to name some `id`'s
* Something something content...
* Need a navbar
* Navbar needs links (I'm not sure what else would go there...)
* Navbar goes on the left and is always visible to a desktop user
* Use a media query

There's not really a reason for me to write any of this down right now. I can 
always just refer back to the document if I need it. 

Now I need some visuals though. Luckily, freeCodeCamp always gives you a link 
to an app that is more or less what you are looking to rebuild. So I follow the 
link and poke around the app. [Here it is if you want it](https://codepen.io/freeCodeCamp/full/NdrKKL).

First thing I notice is it is plain! No worries though because I will style it 
myself. My first idea as far as styling goes is to make the navbar darker than 
the content. Hopefully that makes it pop and draws your eyes there. I also like 
the giant headings there are using here, so I'll keep that in mind. 

So after clicking around for a little bit, it's clear that this one is pretty 
straight forward: make a site with a fixed navbar and links that go to certain 
sections of the overall page.

This is great, I'm off to a good start. I have a sense of the structural 
requirements. I also have an idea or two for the visual component. 

## Putting pen to paper
Depending on my mood, this next step varies. I could be really inspired by a 
site already and want to plan out my layout and styling for 3 viewports 
(Desktop, tablet, and tiny phone). Or I want to think about the structure by 
digging into the User Stories again.

Today is a structure kind of day, but who knows, I might get to the layout and 
design too.

### My Approach: Structure
I've found that mind maps are a pretty good way to think about the structure. 
Somewhere on a piece of paper- I use paper because in the last 4 years of 
coding, I have learned the hard way that I need to slow down with some paper 
first or risk hours wasted on reworking my code repeatedly. So somewhere on 
some paper, I'll start with an `HTML` "node" at the root of the graph. From 
there I link to it exactly like I would write my HTML file: there's a `Head` 
node and a `Body` node. The `Body` gets two children as well: `Main` and 
`Footer`. You get the idea. Branch out until the User Stories are all covered. 
My system is nodes are circles, representing actually blocks of HTML. Lines 
coming off a node without a circle are properties or attributes about that node.

For example, the `Main` node can have three `Section` nodes coming off of it. 
Each of these `Section`s get a line coming off of it for an `id`, or `class`es 
if I'm ready for that, etc. Again, the point of this part is to get a visual 
representation of the HTML I will write that covers the User Stories that it 
can.

Time to fill in the details!

