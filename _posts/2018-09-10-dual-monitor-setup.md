---
title: Dual Monitor Setup
date: 2018-09-10 15:04:22 -0500
tags: []
excerpt_separator: <!--more-->
---

Today was a lot of fiddling with `xrandr` in order to get my laptop screen and
external monitor to play nice together.

While it was a bit confusing at first, the [article on ArchWiki about
HiDPI](https://wiki.archlinux.org/index.php/HiDPI#Side_display) was very useful.
There wasn't a command on the page that was exactly what I needed, but between
the wiki and the man pages, I was able to piece together what I needed.

Here's how I figured out the numbers I needed to create a HiDPI display and a
not HiDPI display that sit next to each other. My exact setup is as follows:
* My `primary` display is named `eDP-1`, has a resolution of `2560x1600`, and a
  DPI of `227` pixels per inch
* My other display is named `DP-1`, has a resolution of `1920x1080`, and a DPI
  of `166` pixels per inch
* I want my other display to the `right-of` my primary one

To get the DPI of my displays, I used [this site](http://dpi.lv/), and just
opened it in Chrome on each monitor.

To get the resolution of my displays, `xrandr` will tell you that if you just
type `xrandr` into the terminal. It displays a little `*` next to the
resolution. This is also how I got the name of each display too.

From the ArchWiki, I grabbed the following formula:
```
xrandr --output eDP-1 --auto --output HDMI-1 --auto --panning [C*E]x[D*F]+[A]+0
--scale [E]x[F] --right-of eDP-1
```

I had to make the following modifications though:
```
xrandr --dpi 227 --output eDP-1 --auto --output DP-1 --auto --panning
3840x2160+2560+0 --scale 2x2 --right-of eDP-1
```

Here's what those changes mean:
* `--dpi 227` is the higher DPI value between the two monitors I am using
* `--output DP-1` was changed from `--output HDMI-1` because that's what my
  display is named
* `--panning 3840x2160+2560+0` uses the following values for `A,C,D,E,F`:
    * A: 2560 (width of the primary display)
    * C: 1920 (width of the other display)
    * D: 1080 (height of the other display)
    * E: 2 (width scale factor)
    * F: 2 (height scale factor)
* `--right-of` means `DP-1` should go on the right of `eDP-1`

The `--panning` was super helpful because otherwise, there's a bug in `xorg`
that makes it so you can't reach half of the other display. It's going to be
fixed in `xorg 1.20`, which is coming with Ubuntu 18.10 I think.

In the mean time, `--panning` works great. Also, the scaling was a bit arbitrary
I think, but `--scale 2x2` was a result of trial and error and it looks great.
To my understanding you can do decimal values, which I didn't experiment with.

The last thing to do with all of htis is to throw my modified command into a
shell script and thell `i3` to run it on startup.

Oh and this whole `xrandr` thing has messed up the wallpaper of my other
display. I think it's definitely the `scale` option, but I'll either fix it
another day by tweaking the i3 settings or I'll find a tiliing wallpaper. 

With that, I am almost done tweaking this Ubuntu install! Things are coming
along pretty nice. 
