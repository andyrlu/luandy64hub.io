---
title: Writing In Org Mode
layout: post
---

## Changing `.spacemacs`

Let me walk you through getting a fresh install of spacemacs to writing your first
blog post.

1.  Follow the these instructions to download spacemacs 
    -   For me, I went to the [Spacemacs](<https://github.com/syl20bnr/spacemacs>)
        GitHub page and followed the instructions for homebrew.

2.  Open spacemacs or your text editor of choice
    -   This step is pretty simple right? We have to edit some text, so we open our
        favorite text editor

3.  Open your `~/.spacemacs` file
    -   This is where your settings live

4.  Find the bit of code that looks like this:
    If it doesn't exist already, go ahead and just add this yourself.
    
    ```
  dotspacemacs-configuration-layers
  '(
        ;; Lots of stuff in this set of parentheses
  )
  ```

5.  Add the following snippet of code anywhere inside of the parentheses

    ```
    (org :variables
         org-enable-github-support t
         org-export-with-toc nil)
    ```
    
    After you add that in, assuming you did it at the end of whatever is already
    there, it should look like this:
    
    ```
  dotspacemacs-configuration-layers
  '(
        ;; Lots of stuff in this set of parentheses
        (org :variables
             org-enable-github-support t
             org-export-with-toc nil)
  )
  ```
    
    So three things that I know of are happening here. The line that starts with
    `(org` says, "Add org-mode as a mode I can use in Emacs". The 
    `github-support` line says, "Let me export the file in the way Github likes
    it". Finally, the `export-with-toc` says, "Don't include a table of contents
    in any file I export".
    
    There's one more line we need to add before this works though.

6.  Find this line of code or add it yourself:

    ```
  dotspacemacs-additional-packages '()
  ```

7.  In the parentheses, add this bit of text

    ```
  ox-gfm
  ```
    
    This is a package that lets Org-mode export to GitHub Flavored Markdown, on
    top of all of the other formats it can already handle.

8.  Save your file
    - In evil mode, that would be `SPC f s`.

9.  Reload the init file
    - In evil mode, that would be `SPC f e R`

### That's all folks!

