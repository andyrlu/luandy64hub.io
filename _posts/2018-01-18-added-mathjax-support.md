---
excerpt_separator: <!--more-->
icon: fa-arrow-circle-right
---
##  Commands Needed
The easiest step was from the [Jekyll Docs](https://jekyllrb.com/docs/extras/), which pointed me to this page on [Visually Enforced](http://www.gastonsanchez.com/visually-enforced/opinion/2014/02/16/Mathjax-with-jekyll/). So the following piece of code went into my `/_layouts/default.html` 's header:
<!--more-->
```
<script type="text/javascript"
    src="http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML">
</script>
```

Testing the site was too slow with GitHub Pages, so I served the site locally to see changes on every refresh:
`bundle exec jekyll serve`

## Links on this page
- [Jekyll Docs](https://jekyllrb.com/docs/extras/)
- [Visually Enforced](http://www.gastonsanchez.com/visually-enforced/opinion/2014/02/16/Mathjax-with-jekyll/)
- [kramdown](https://kramdown.gettalong.org/syntax.html#math-blocks)
- [Supported LaTeX Commands](https://docs.mathjax.org/en/latest/tex.html#supported-latex-commands)
- [StackExchange Proof QED](https://math.meta.stackexchange.com/questions/3582/qed-for-mathjax-here-on-stackexchange/13338#13338)

- - - -

## The Details
To make sure everything worked after adding the MathJax line, I found a nice chunk of code from [kramdown](https://kramdown.gettalong.org/syntax.html#math-blocks) in their syntax guide. The $$\LaTeX$$ rendered beautifully and had no issues whatsoever. I had done some more digging and saw that kramdown already supports "Math Blocks", so to test it, I removed the MathJax script from my template. It broke the nice piece of $$\LaTeX$$ code from earlier, so I quickly put the script back. 

The next test I wanted to try was to see what commands were available to me because I couldn't find a list anywhere. So I grabbed a proof I wrote from my Real Analysis class and threw it into a Markdown file. It kept rendering it as text in a box; basically like I used some CSS to give that chunk of text an outline. From there, I kept playing with it, in hopes that something would work. kramdown said that everything should be in `$$...$$`, which I tried and only got a group of -box-outlined text. Then I thought maybe I need to import the AMS Math package somewhere. It was after that thought, I think, that I stumbled upon a list of [Supported LaTeX Commands](https://docs.mathjax.org/en/latest/tex.html#supported-latex-commands)...

So after all of that work, it turns out the *proof* environment I was using is unsupported. That's alright though, since that was mainly for the TeX engine to put a fancy italic *Proof* at the start of my work and a box at the end. I can format the *Proof* first, and probably put the black box at the end. 

*Proof.* Look it worked. [Credit](https://math.meta.stackexchange.com/questions/3582/qed-for-mathjax-here-on-stackexchange/13338#13338) $$\tag*{$\blacksquare$}$$


Anyway, I'll have to spend some time looking through what commands I can and cannot use, but I am happy to have them at all. I doubt anyone who wrote the pages that helped me in this process will ever see this page, but thank you!

## In Summary
It was a little confusing at first, but so was setting up Jekyll in the first place. The confusion was mainly due to the fact that I had to dig through Jekyll support pages, kramdown support pages, and MathJax support pages. In the end though, it just took some trial and error on a local version of my site to figure out what works and what doesn't. 